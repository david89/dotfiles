# _____ ___ ____  _   ___   __
#|  ___|_ _/ ___|| | | \ \ / /
#| |_   | |\___ \| |_| |\ V /
#|  _|  | | ___) |  _  | | |
#|_|   |___|____/|_| |_| |_|
#
# Updated 2021-02-23

#disable greeting

set fish_greeting

#exports

#export PAGER=vimpager
export PAGER="nvim -R +'set ft=man' +'set number relativenumber' -"
export EDITOR="nvim"

#prompt

function fish_mode_prompt
# Do nothing if not in vi mode
    if test "$fish_key_bindings" = "fish_vi_key_bindings"
        or test "$fish_key_bindings" = "fish_hybrid_key_bindings"
        switch $fish_bind_mode
#default
            case default
    if test -n "$SSH_TTY"
        echo -n (set_color brred)"$USER"(set_color white)'@'(set_color yellow)(prompt_hostname)' '
    end

    echo -n (set_color blue)(prompt_pwd)' '

    if test "$USER" = 'root'
        echo -n (set_color red)'# '
    end
    #echo -n (set_color green)'❯'(set_color yellow)'❯'(set_color red)'❯ '
    echo -n (set_color magenta)'><(((º>'
    set_color normal
#insert
            case insert

     echo -n (set_color blue)(prompt_pwd)' '

   if test "$USER" = 'root'
        echo -n (set_color red)'# '
    end
    #echo -n (set_color red)'❯'(set_color yellow)'❯'(set_color green)'❯ '
    echo -n (set_color green)'><(((º>'
    set_color normal

            case replace_one

      echo -n (set_color blue)(prompt_pwd)' '

  #echo -n (set_color red)'❯'(set_color yellow)'❯'(set_color green)'❯ '
    echo -n (set_color cyan)'><(((º>'
    set_color normal


            case replace
       echo -n (set_color blue)(prompt_pwd)' '

 #echo -n (set_color red)'❯'(set_color yellow)'❯'(set_color green)'❯ '
    echo -n (set_color red)'><(((º>'
    set_color normal



#visual
            case visual
    		echo -n (set_color blue)(prompt_pwd)' '
     		echo -n (set_color yellow)'><(((º>'
        end
        set_color normal
        echo -n ' '
    end
end

function fish_prompt
	#echo -n (set_color blue)(prompt_pwd)' '
	#echo -n (set_color green)'><(((º>'
end

#right side prompt
function fish_right_prompt
	set -l cmd_status $status
        if test $cmd_status -ne 0
            echo -n (set_color red)"✘ $cmd_status"
	    echo -n (set_color red)'¸.·´¯`·.´¯`·.¸¸.·´¯`·.¸><(((º>'
	else
	    echo -n (set_color green)'<º)))><'
        end

    	#echo -n (set_color red)'<'(set_color yellow)'<'(set_color green)'< '
	#echo -n (set_color green)'<\')))><'
    	set_color normal

end

#enable !! and !$ -- BREAKS IN VI MODE

function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

bind ! __history_previous_command
bind '$' __history_previous_command_arguments

#enable vi keybindings
fish_vi_key_bindings

#enable default keybindings
#fish_default_key_bindings

#aesthetic

neofetch

#aliases

abbr please "sudo"
abbr neo "neofetch"
abbr up "uptime"
abbr dumb "cowsay | lolcat"
abbr confish "nvim ~/.config/fish/config.fish"
abbr conkitty "nvim ~/.config/kitty/kitty.conf"
abbr convim "nvim ~/.config/nvim/init.vim"
abbr lsf "ls -lah"
#abbr cal "calcurse"
abbr rm "rm -i"
abbr mv "mv -v"
abbr cp "cp -v"
abbr mkd "mkdir -p"
abbr v "nvim"
abbr vim "nvim"
abbr icat "kitty +kitten icat --align=left"
abbr egrep "egrep --color=AUTO"
abbr aptup "sudo apt update && apt list --upgradable && sudo apt upgrade -y && flatpak update"
abbr aptc "sudo apt clean && sudo apt autoremove"
abbr aptupc "sudo apt update && apt list --upgradable && sudo apt upgrade -y && flatpak update && sudo apt clean && sudo apt autoremove"
