#!/bin/bash

cp -v ./.zshrc ~/.zshrc
cp -v ./.p10k.zsh ~/.p10k.zsh
cp -v ./.Xresources ~/.Xresources
cp -v ./nvim/init.vim ~/.config/nvim/init.vim
cp -v ./terminator/config ~/.config/terminator/config
cp -v ./fish/config.fish ~/.config/fish/config.fish
