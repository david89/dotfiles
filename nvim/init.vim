"__     _____ __  __ ____   ____
"\ \   / /_ _|  \/  |  _ \ / ___|
" \ \ / / | || |\/| | |_) | |
"  \ V /  | || |  | |  _ <| |___
"   \_/  |___|_|  |_|_| \_\\____|

let mapleader = " "

"Plugins!!
call plug#begin('~/.vim/plugged')

Plug 'itchyny/lightline.vim'
Plug 'machakann/vim-highlightedyank'
Plug 'Kjwon15/vim-transparent'
Plug 'dracula/vim',{'as':'dracula'}
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'machakann/vim-swap'
Plug 'fatih/vim-go'
Plug 'bfrg/vim-cpp-modern'
Plug 'rhysd/vim-clang-format'

call plug#end()

"Set lightline color scheme
let g:lightline = {
	\ 'colorscheme': 'darcula',
	\ }

"Set color scheme
set termguicolors
set background=dark
colorscheme dracula

"Go stuff
let g:go_highlight_types = 1
let g:go_highlight_functions = 1
let g:go_highlight_fields = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_operators = 1

"C++ stuff
let g:cpp_attributes_highlight = 1
let g:cpp_member_highlight = 1
let g:cpp_simple_highlight = 1

filetype indent on
set number relativenumber    "Numbers on the left with absolute line number on current line
set wildmode=longest,list,full   "Command mode autocompletion
set splitbelow splitright    "Splits open below or on the right
autocmd BufWritePre * %s/\s\+$//e    "Automatically delete any trailing whitespace
autocmd InsertEnter * norm zz	 "Auto-Center on enter insert
set mouse=a
syntax on
set ignorecase
set smartcase
set cursorline
set cursorcolumn

"Wrapper autosandwiching
inoremap ( ()<Esc>i
inoremap (<CR> (<CR>)<Esc>O
inoremap { {}<Esc>i
inoremap {<CR> {<CR>}<Esc>O
inoremap [ []<Esc>i
inoremap ' ''<Esc>i
inoremap " ""<Esc>i

inoremap -- <esc>la
inoremap ;/ <esc>A//
inoremap ;; <esc>A;<esc>

"microsnippets
inoremap ;o cout <<  << endl;<esc>8hi

map <leader>s a <esc>
map <leader>cb 0i#<esc>
map <leader>cc 0i//<esc>
map <leader>ub 0x
map <leader>uc 0xx
map <leader>cf :ClangFormat<CR>
map <leader>; A;<esc>
map <leader>l :nohl<CR>
map <leader>q :q!<CR>
map <leader>w :w<CR>
map <leader>b o<esc>
map <leader>d :set filetype?<CR>
