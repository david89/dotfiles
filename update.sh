#!/bin/bash

cp -v ~/.zshrc ./.zshrc
cp -v ~/.p10k.zsh ./.p10k.zsh
cp -v ~/.Xresources ./.Xresources
cp -v ~/.config/nvim/init.vim ./nvim/init.vim
cp -v ~/.config/terminator/config ./terminator/config
cp -v ~/.config/fish/config.fish ./fish/config.fish
